group { 'puppet': ensure => present }
Exec { path => [ '/bin/', '/sbin/', '/usr/bin/', '/usr/sbin/' ] }
File { owner => 0, group => 0, mode => 0644 }

class {'apt':
  always_apt_update => true,
}

Class['::apt::update'] -> Package <|
    title != 'python-software-properties'
and title != 'software-properties-common'
|>

    apt::key { '4F4EA0AAE5267A6C': }

apt::ppa { 'ppa:ondrej/php5-oldstable':
  require => Apt::Key['4F4EA0AAE5267A6C']
}

class { 'puphpet::dotfiles': }

package { [
    'build-essential',
    'vim',
    'curl',
    'git-core'
  ]:
  ensure  => 'installed',
}

class { 'sendmail': }

class { 'apache': }

apache::dotconf { 'custom':
  content => 'EnableSendfile Off',
}

apache::module { 'rewrite': }

apache::vhost { 'awesome.dev':
  server_name   => 'awesome.dev',
  serveraliases => [
    'awesome.dev'
    ],
  docroot       => '/var/www/docroot',
  port          => '80',
  env_variables => [
],
  priority      => '1',
}

class { 'php':
  service             => 'apache',
  service_autorestart => false,
  module_prefix       => '',
}

php::module { 'php5-mysql': }
php::module { 'php5-cli': }
php::module { 'php5-common': }
php::module { 'php5-curl': }
php::module { 'php5-gd': }
php::module { 'php5-intl': }
php::module { 'php5-mcrypt': }
php::module { 'php5-fpm': }
php::module { 'php-apc': }

class { 'php::devel':
  require => Class['php'],
}

class { 'php::pear':
  require => Class['php'],
}

class { 'composer':
  require => Package['php5', 'curl'],
}

puphpet::ini { 'php':
  value   => [
    'date.timezone = "Europe/London"'
  ],
  ini     => '/etc/php5/conf.d/zzz_php.ini',
  notify  => Service['apache'],
  require => Class['php'],
}

puphpet::ini { 'custom':
  value   => [
    'display_errors = On',
    'error_reporting = E_ALL',
    'display_html_errors = On',
    'memory_limit = 256M',
    'max_execution_time = 180'
  ],
  ini     => '/etc/php5/conf.d/zzz_custom.ini',
  notify  => Service['apache'],
  require => Class['php'],
}


class { 'mysql::server':
  config_hash   => { 'root_password' => 'password' }
}

# PEAR
exec { "pear upgrade":
  command => "/usr/bin/pear upgrade",
  require => Package['php-pear'],
  returns => [ 0, '', ' ']
}

# set channels to auto discover
exec { "pear auto_discover" :
  command => "/usr/bin/pear config-set auto_discover 1",
  require => [Package['php-pear']]
}

exec { "pear update-channels" :
  command => "/usr/bin/pear update-channels",
  require => [Package['php-pear']]
}

exec { "pear install phpunit":
  command => "/usr/bin/pear install --alldeps PHPUnit",
  creates => "/usr/share/php/PHPUnit/Assert.php",
  require => Exec['pear update-channels']
}

exec { "pear install Console_Table":
  command => "/usr/bin/pear install --alldeps Console_Table",
  creates => "/usr/share/php/Console/Table.php",
  require => Exec['pear update-channels']
}

# SSH Keys for Build server
class ssh_keys {

  file { '/home/vagrant/.ssh':
      ensure => directory,
      mode => '0700',
      owner => 'vagrant',
      group => 'vagrant',
  }

  file { '/root/.ssh':
      ensure => directory,
      mode => '0700',
      owner => 'root',
      group => 'root',
  }

  file { "/home/vagrant/.ssh/id_rsa":
    source => "puppet:///modules/ssh_keys/vagrant_id_rsa"
  }

  file { "/root/.ssh/id_rsa":
    source => "puppet:///modules/ssh_keys/vagrant_id_rsa"
  }
}
include ssh_keys

# Gems
package { "bundler":
    ensure => "installed",
    provider => "gem",
}

package { "sass":
    ensure   => "installed",
    provider => "gem",
}

package { "susy":
    ensure   => "installed",
    provider => "gem",
}

# Node JS
apt::ppa { "ppa:chris-lea/node.js": }
class { 'nodejs':
  require => Apt::Ppa["ppa:chris-lea/node.js"]
}
